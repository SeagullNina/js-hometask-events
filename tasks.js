'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/
function openMenu() {
  document.getElementById('menu').style.display='block';
}

function closeMenu(){
  document.getElementById('menu').style.display='none';
}

menuBtn.addEventListener("click", openMenu);
menu.addEventListener("click", closeMenu);

/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/
function moveBlock(event){
  document.getElementById('movedBlock').style.top = event.clientY -`${document.getElementById('field').getBoundingClientRect().top}` + 'px';
  document.getElementById('movedBlock').style.left = event.clientX -`${document.getElementById('field').getBoundingClientRect().top}` + 'px';
  console.log(document.getElementById('movedBlock').style.left, document.getElementById('movedBlock').style.top)
}

field.addEventListener("click", moveBlock);

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/
function removeMessage(event) {
  const { target } = event
  const isRemove = target.classList.contains('remove');
  if (!isRemove) return;
  event.target.parentNode.style.visibility = 'hidden';
}
messager.addEventListener("click", removeMessage);

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/
  function openMessage(event) {
    let link = event.target.closest('a');
    if (!link) return;
    const answer = confirm('Вы уверены, что хотите перейти по ссылке?');
    if (answer) {
      location.href = link.href;
    } else {
      event.preventDefault();
    }
  }
  links.addEventListener("click", openMessage);

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/
function changeHeader(){
  taskHeader.innerHTML = fieldHeader.value;
}
fieldHeader.addEventListener("keydown", changeHeader);
